import java.io.Serializable;


public class Command implements Serializable{
	private String message;
	private int numReplies;
	private int numExpected;
	private int timestamp;
	
	public Command(String mess, int numE, int timeS){
		message = mess;
		numReplies = 0;
		numExpected = numE;
		timestamp = timeS;
	}
}

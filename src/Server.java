import java.net.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.*;
import java.io.*;

public class Server {

	static HashMap<String, String> library = new HashMap<String, String>();
	static ArrayList<String> addresses = new ArrayList<String>();
	static LinkedList<String> crashes = new LinkedList<String>();
	static Queue<String> messages = new LinkedList<String>();
	
	static int numServer;
	static int id;
	static int clock;

	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		clock = 0;
		String initialIn = null;
		try {
			initialIn = br.readLine();
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		id = Integer.parseInt(initialIn.split("\\s+")[0]);
		id = id - 1;
		numServer = Integer.parseInt(initialIn.split("\\s+")[1]);
		int books = Integer.parseInt(initialIn.split("\\s+")[2]);

		//gets addresses
		for (int i = 0; i < numServer; i++) {
			try {
				addresses.add(br.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		for (int i = 1; i <= books; i++) {
			library.put("b" + i, "");
		}
		
		//gets crashes
		String inLine = "";
		try {
			while ((inLine = br.readLine()) != null) {
				crashes.add(inLine);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			String ip = addresses.get(id).split(":")[0].trim();
			int tP = Integer.parseInt(addresses.get(id).split(":")[1].trim());
			System.out.println("ip is " + ip );
			System.out.println("tP is " + tP );
			
			//SocketAddress tport = new InetSocketAddress(tP);
			//ServerSocketChannel tserver = ServerSocketChannel.open();
			//tserver.socket().bind(tport);
			String[] argList = {"s", ""+id,""+numServer, ""+tP,ip};
			Linker link = new Linker(argList);
			CharsetEncoder encode = Charset.forName("US-ASCII")	.newEncoder();
			SocketAddress tport = new InetSocketAddress(tP);
			//while ((inLine = br.readLine()) != null) {
			int count = 0;
			while (true) {
				
				if(!crashes.isEmpty()){
					String crash = crashes.getFirst();
				
					int limit = Integer.parseInt(crash.split("\\s+")[1]);
					int time = Integer.parseInt(crash.split("\\s+")[2]);
				
					if (count >= limit){
						crash(time);
						crashes.removeFirst();
						recover();
					}
				}
				try {
					//SocketChannel client = tserver.accept();
					link.init(link);
					Socket s = link.connector.listener.accept();
					OutputStream os = s.getOutputStream();
					BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

					String cMess;
					String rtnString = "";
					if ((cMess = in.readLine()) != null) {
						rtnString = doWork(cMess);
					}
					ByteBuffer response = encode.encode(CharBuffer.wrap(rtnString));
					if (s != null) {
						if(rtnString.equals("recover")){
							ObjectOutputStream  oos = new ObjectOutputStream(s.getOutputStream());
				    		oos.writeObject(library);
				    		oos.close();
						}
						else{
							//////
							//lamp(link,tP);
							os.write(response.array());
							s.close();
							count ++;
						}
					}
								
				} catch (Exception e) {
					// System.err.println(e);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	public static void lamp(Linker link, int port){
		

		try {
			
			LamportMutex lampMut = new LamportMutex(link);
			lampMut.requestCS();
			System.out.println("DOSTUFF");
			lampMut.releaseCS();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void checkOthers(String cMessage){
		//iterate through server list and connect to one, then copy over library. I think should be safe since ports can only be used one at a time. 
				//http://www.coderpanda.com/java-socket-programming-transferring-of-java-objects-through-sockets/
				clock++;
				int count = 0;
				for (String address : addresses){
					try {
						// get the address by count and cannot be itself
						if(count == id){
							count++;
							continue;
						}
						
						
						//System.out.println(address);

						String ipStr = address.split(":")[0].trim();
						String[] ipBytes = ipStr.split("\\.");
						byte[] ip = new byte[ipBytes.length];
						for (int i = 0; i < ipBytes.length; i++) {
							ip[i] = (byte) Integer.parseInt(ipBytes[i]);
						}

						InetAddress ia = InetAddress.getByAddress(ip);
						int port = Integer.parseInt(address.split(":")[1].trim());

						Socket s = new Socket(ia, port);
						s.setSoTimeout(100);

						PrintWriter out = new PrintWriter(s.getOutputStream(),true);
						BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
						ObjectOutputStream  oos = new ObjectOutputStream(s.getOutputStream());
						Command com = new Command(cMessage ,0,clock);
						oos.writeObject(com);
						oos.close();
						count++;
					} catch (SocketTimeoutException | ConnectException e1) {
						System.out.println("need to find another server");
						count++;
					} catch (IOException e1) {
						System.err.println(e1);
						e1.printStackTrace();
					}
				}
	}
	public static void crash(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		library.clear();
	}
	
	public static void recover(){
		//iterate through server list and connect to one, then copy over library. I think should be safe since ports can only be used one at a time. 
		//http://www.coderpanda.com/java-socket-programming-transferring-of-java-objects-through-sockets/
		int count = 0;
		while (true) {
			try {
				// get the address by count and cannot be itself
				if(count == id){
					count ++;
				}
				
				String address = addresses.get(count % numServer);
				
				//System.out.println(address);

				String ipStr = address.split(":")[0].trim();
				String[] ipBytes = ipStr.split("\\.");
				byte[] ip = new byte[ipBytes.length];
				for (int i = 0; i < ipBytes.length; i++) {
					ip[i] = (byte) Integer.parseInt(ipBytes[i]);
				}

				InetAddress ia = InetAddress.getByAddress(ip);
				int port = Integer.parseInt(address.split(":")[1].trim());

				Socket s = new Socket(ia, port);
				s.setSoTimeout(100);

				PrintWriter out = new PrintWriter(s.getOutputStream(),true);
				BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
				String message = "s 0 recover\n";
				out.println(message);
				out.flush();
				
				ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
				library =(HashMap<String, String>) ois.readObject();
				System.out.println("recovered");
		
				break;

			} catch (SocketTimeoutException | ConnectException e1) {
				System.out.println("need to find another server");
				count++;
			} catch (IOException | ClassNotFoundException e1) {
				System.err.println(e1);
				e1.printStackTrace();
			}
		}
				
		
	}
	
	public static String doWork(String input) {
		String[] datastr = input.split("\\s+");
		String clientID = datastr[0];
		String bookNum = datastr[1];
		String cmdType = datastr[2];
		String rtnString = "";
		if (cmdType.equalsIgnoreCase("reserve")) {
			String cID = library.get(bookNum);
			if (cID == null || !cID.equals("")) {
				rtnString = "fail " + clientID + " " + bookNum;
			} else {
				library.put(bookNum, clientID);
				rtnString = "" + clientID + " " + bookNum;
			}
		} else if (cmdType.equalsIgnoreCase("return")) {
			String cID = library.get(bookNum);
			if (cID.equals(clientID)) {
				library.put(bookNum, "");
				rtnString = "free " + clientID + " " + bookNum;
			} else {
				rtnString = "fail " + clientID + " " + bookNum;
			}
		} else if (cmdType.equalsIgnoreCase("recover")){
			rtnString = "recover";
		}
		return rtnString;
	}

}

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Client {
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String id = "";
		String ipStr = "";

		int numServer;
		ArrayList<String> addresses = new ArrayList<String>();

		String initialIn = null;
		try {
			initialIn = br.readLine();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		id = initialIn.split("\\s+")[0];
		numServer = Integer.parseInt(initialIn.split("\\s+")[1]);

		// put the ports and addresses into list, havent split them yet
		for (int i = 0; i < numServer; i++) {
			try {
				addresses.add(br.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		int count = 0;

		String inLine = "";
		try {
			while ((inLine = br.readLine()) != null) {
				while (true) {
					try {
						// get the address by count
						String address = addresses.get(count % numServer);
						//System.out.println(address);

						ipStr = address.split(":")[0].trim();
						String[] ipBytes = ipStr.split("\\.");
						byte[] ip = new byte[ipBytes.length];
						for (int i = 0; i < ipBytes.length; i++) {
							ip[i] = (byte) Integer.parseInt(ipBytes[i]);
						}

						InetAddress ia = InetAddress.getByAddress(ip);
						int port = Integer.parseInt(address.split(":")[1].trim());

						String[] commands = inLine.split("\\s+");

						if (commands[0].equalsIgnoreCase("sleep")) {
							long time = Long.parseLong(commands[1]);
							Thread.sleep(time);
		
						} else {
							String bookID = commands[0];
							String cmdType = commands[1];

							Socket s = new Socket(ia, port);
							s.setSoTimeout(100);

							PrintWriter out = new PrintWriter(s.getOutputStream(),
									true);
							BufferedReader in = new BufferedReader(
									new InputStreamReader(s.getInputStream()));
							String message = id + " " + bookID + " " + cmdType
									+ "\n";
							out.println(message);
							out.flush();
							String rMess;
							while ((rMess = in.readLine()) == null) {};
							System.out.println(rMess);
		
						}
						break;

					} catch (SocketTimeoutException | ConnectException e1) {
						System.out.println("need to find another server");
						count++;
					} catch (IOException |InterruptedException e1) {
						System.err.println(e1);
						e1.printStackTrace();
					}
				}
			}
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
	}
}